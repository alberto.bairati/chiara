#include <Ultrasonic.h>

Ultrasonic ultrasonic(7, 6);

const int redPin = 11;
const int greenPin = 10;
const int bluePin = 9;

// Set ultrasonic range in cm

const int minDistance = 10;
const int maxDistance = 41;  

int n = 1;

void setup()
{
    pinMode(redPin, OUTPUT);
    pinMode(greenPin, OUTPUT);
    pinMode(bluePin, OUTPUT);
    Serial.begin(9600);

    // Check the 3 colors (R -> G -> B)
    // Are the connections wired in the right order?
    
    setRgb(50, 0, 0);
    delay(500);
    setRgb(0, 50, 0);
    delay(500);
    setRgb(0, 0, 50);
    delay(500);
    setRgb(0, 0, 0);
}

void loop()
{
    int distance = ultrasonic.distanceRead();
    if (distance <= maxDistance && distance > minDistance) {
        n = (distance - minDistance) / 4;
        Serial.print("num: ");
        Serial.println(n);
        Serial.print("distance: ");
        Serial.println(distance);
        selectColor(n);
    }
    delay(300);
}

void selectColor(unsigned char color)
{
    switch (color)
    {
        case 1:
            setRgb(255, 255, 255);  // white
            break;
        case 2:
            setRgb(0, 255, 255);    // cyan
            break;
        case 3:
            setRgb(0, 0, 255);      // blue
            break;
        case 4:
            setRgb(0, 255, 0);      // green
            break;
        case 5:
            setRgb(255, 255, 0);    // yellow
            break;
        case 6:
            setRgb(255, 0, 0);      // red
            break;
        case 7:
            setRgb(255, 0, 255);    // purple
            break;  
        default:
            setRgb(0, 0, 0);        // turn LEDs off
            break;
    }
}

void setRgb (unsigned char red, unsigned char green, unsigned char blue)
{
    analogWrite(redPin, red);
    analogWrite(greenPin, green);
    analogWrite(bluePin, blue);
}