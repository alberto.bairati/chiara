# Chiara

> An hands-free RGB LED abat-jour

**Chiara** (*ˈkjara*, italian female first name, meaning *clear*, *light* or *bright*) is a RGB LED light with a control interface designed to be as minimal as possible. In effect, it has no physical control, because user can simply switch the light or change its color without touching anything, just floating his hand in the air, over the lamp. This project can also be considered as **an hack of the famous and cheap *Ikea Lampan* table light**.

## Components

The lamp uses these components:

- [White Ikea Lampan](http://www.ikea.com/it/it/catalog/products/20046988/)
- [Arduino Uno](https://www.arduino.cc/en/main/arduinoBoardUno)
- Ultrasonic Sensor HC-SR04
- IRLB8721 IR Transistor N-MOSFET
- LED stripe RGB 5050
- 12V power supply

## Wiring

### Ultrasonic sensor

To simplify the usage of the ultrasonic sensor, I make use of the [Ultrasonic library by ErickSimoes](https://github.com/ErickSimoes/Ultrasonic). On its page you can find all the informations to install and to properly connect the sensor to the board.

![Ultrasonic sensor wiring](https://github.com/ErickSimoes/Ultrasonic/raw/master/extras/HC-SR04-with-Arduino.jpg)

### LED stripe

Since the LED are powered by 12V power and Arduino can give you only 5V per pin, the connection between the board and the stripe require three (one per each color) N-MOSFET. I use the guide [RGB LED Stripes Usage by Adafruit](https://learn.adafruit.com/rgb-led-strips/usage) as a reference for this part of the project.

![LED stripe wiring](https://cdn-learn.adafruit.com/assets/assets/000/002/692/original/led_strips_ledstripfet.gif)